﻿using ConsultaCEP.Interfaces;
using ConsultaCEP.Services.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ConsultaCEP.Abstracts
{
    public abstract class CepService : ICEPService
    {
        private string EnderecoURL;
        private readonly string Cep;

        public abstract bool CepValido { get; }

        public CepService(string cep)
        {
            Cep = cep;
        }

        public abstract Endereco BuscarEndereco();

        public abstract Task<Endereco> BuscarEnderecoAsync();
    }
}
