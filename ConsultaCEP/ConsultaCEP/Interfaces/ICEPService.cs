﻿using ConsultaCEP.Services.Model;
using System.Threading.Tasks;

namespace ConsultaCEP.Interfaces
{
    interface ICEPService
    {        
        Endereco BuscarEndereco();
        Task<Endereco> BuscarEnderecoAsync();
    }
}
