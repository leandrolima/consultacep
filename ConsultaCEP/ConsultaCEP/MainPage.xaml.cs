﻿using ConsultaCEP.Services;
using ConsultaCEP.Services.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ConsultaCEP
{
    public partial class MainPage : ContentPage
    {
        private ViaCEPServico viaCep;

        private const string CEP_INVALIDO_OITO_CARACTERES = "CEP Inválido. O CEP deve ter 8 caracteres";
        private const string CEP_INVALIDO_NAO_NUMERICO = "CEP Inválido. O CEP deve conter apenas números";

        public MainPage()
        {
            InitializeComponent();
            btnBuscarCEP.Clicked += BtnBuscarCEP_Clicked;
        }

        private void BtnBuscarCEP_Clicked(object sender, EventArgs e)
        {
            string cep = txtCEP.Text.Trim();
            if (!CepValido(cep))
                return;

            try
            {
                viaCep = new ViaCEPServico(cep);
                Endereco end = viaCep.BuscarEndereco();
                if (end.cep != null)
                    lblCEP.Text = $"Endereço: {end.logradouro}, {end.bairro} - {end.localidade}, {end.uf} ";
                else
                    ErrorAlert($"O endereço não foi encontrado para o CEP informado: {cep}");
            }
            catch (Exception f)
            {
                ErrorAlert(f.Message);
            }
        }

        private bool CepValido(string cep)
        {
            if (cep.Length != 8)
            {
                ErrorAlert(CEP_INVALIDO_OITO_CARACTERES);
                return false;
            }

            if (!int.TryParse(cep, out int novocep))
            {
                ErrorAlert(CEP_INVALIDO_NAO_NUMERICO);
                return false;
            }

            return true;
        }
        private void ErrorAlert(string message)
        {
            DisplayAlert("Erro", message, "OK");
        }
    }
}
