﻿using ConsultaCEP.Abstracts;
using ConsultaCEP.Services.Model;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Threading.Tasks;

namespace ConsultaCEP.Services
{
    public class ViaCEPServico : CepService
    {
        private string EnderecoURL = "http://viacep.com.br/ws/{0}/json/";
        private readonly string Cep;
        private const string CEP_INVALIDO_OITO_CARACTERES = "CEP Inválido. O CEP deve ter 8 caracteres";
        private const string CEP_INVALIDO_NAO_NUMERICO = "CEP Inválido. O CEP deve conter apenas números";

        public string MensagemErroValidacaoCep { get; private set; }
        public override bool CepValido
        {
            get
            {
                if (Cep.Length != 8)
                {
                    MensagemErroValidacaoCep = CEP_INVALIDO_OITO_CARACTERES;
                    return false;
                }

                if (!int.TryParse(Cep, out int novocep))
                {
                    MensagemErroValidacaoCep = CEP_INVALIDO_NAO_NUMERICO;
                    return false;
                }
                MensagemErroValidacaoCep = "";
                return true;
            }
        }

        public ViaCEPServico(string cep) : base(cep)
        {
            this.Cep = cep;
        }
        public override Endereco BuscarEndereco()
        {
            var conteudo = new WebClient().DownloadString(new Uri(string.Format(EnderecoURL, Cep)));
            var endereco = JsonConvert.DeserializeObject<Endereco>(conteudo);
            return endereco.cep == null ? null : endereco;
        }

        public override async Task<Endereco> BuscarEnderecoAsync()
        {
            var conteudo = await new WebClient().DownloadStringTaskAsync(new Uri(string.Format(EnderecoURL, Cep)));
            return JsonConvert.DeserializeObject<Endereco>(conteudo);
        }
    }
}
